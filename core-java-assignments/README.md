**Core-Java-Assignment**

**Project Description --------------------------------------------------------------**
Revature is building a new API! This API contains methods for validating data,
solving problems, and encoding data.

- -Without using the StringBuilder or StringBuffer class, write a method that reverses a String.
- -Convert a phrase to its acronym.
- -Determine if a triangle is equilateral, isosceles, or scalene.
- -Given a word, compute the scrabble score for that word.
- -Clean up user-entered phone numbers so that they can be sent SMS messages.
- -Given a phrase, count the occurrences of each word in that phrase.
- -Implement a binary search algorithm.
- -Create an implementation of the rotational cipher, also sometimes called the Caesar cipher.
- -Given a number n, determine what the nth prime is.
- -Determine if a sentence is a pangram.


As solutions are implemented, we verified that our methods are working properly by running the tests 
in the EvaluationServiceTest class. We ran the tests to check workability. 
This was done with the green run arrows beside each test method, and the double green arrow beside the class, respectively.

**Technologies Used --------------------------------------------------------------**

- -IntelliJ
- -Git Bash


**Getting Started ----------------------------------------------------------------**

Requirements : Git, GitLab Account, IDE/Text Editor

Clone this repository locally.
Open a terminal shell or git-scm terminal and clone the repository to your computer using the Git command:

git clone https://github.com/[OrganizationName]/[RepositoryName].git


Next, open the project with your preferred text editor or IDE and check the assignment according to each question's instructions. If using IntelliJ, open the project by selecting 'open,' navigating to the 'core-java-assignment' file directory, and selecting the pom.xml file. Your IDE should give you the option to open as a project, select that option.

You can verify that the methods are working properly by running the tests in the EvaluationServiceTest class. You can do this, by running the tests individually, or running the entire test class. This is done with the green run arrows beside each test method, and the double green arrow beside the class, respectively.

Be sure not to alter the folder structure, configuration files, or unit tests of the assignment unless specifically told to do so by your trainer. The completed assignment should be pushed to your GitLab repo by the end of the day Monday, Februrary 28, 2022.
